# Up1 - Puppet Module
# Client side encrypted storage/paste
# https://github.com/Upload/Up1
# 
# Requires:
#   https://github.com/puppetlabs/puppetlabs-vcsrepo
#
class up1 (
  $repository_remote      = $up1::params::repository_remote,
  $repository_revision    = $up1::params::repository_revision,
  $default_user           = $up1::params::default_user,
  $default_group          = $up1::params::default_group,
  $default_path           = $up1::params::default_path,
  $systemd_template       = $up1::params::systemd_template,
  $app_api_key            = $up1::params::app_api_key,
  $app_delete_key         = $up1::params::app_delete_key,
  $app_max_file_size      = $up1::params::app_max_file_size,
  $app_email              = $up1::params::app_email,
  $app_http_port          = $up1::params::app_http_port,
  $app_https_port         = $up1::params::app_https_port,
  $app_https_cert         = $up1::params::app_https_cert,
  $app_https_key          = $up1::params::app_https_key,
  $cloudflare_token       = $up1::params::cloudflare_token,
  $cloudflare_email       = $up1::params::cloudflare_email,
  $cloudflare_domain      = $up1::params::cloudflare_domain
) inherits up1::params {

  # Up1 - Instalation and configuration
  anchor { 'up1::begin': } ->
  class { '::up1::install': } ->
  class { '::up1::config': } ->
  class { '::up1::service': } ->
  anchor { 'up1::end': }

}