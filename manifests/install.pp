# Up1 Install
class up1::install inherits up1 {

  ensure_packages(['git', 'golang'])

    group { $default_group:
        ensure    => 'present',
        allowdupe => false,
    }

    user { $up1::default_user:
        ensure    => 'present',
        allowdupe => false,
        gid       => $up1::default_user,
        require   => Group[$up1::default_group],
    }

    file { $up1::default_path:
        ensure  => directory,
        owner   => $up1::default_user,
        group   => $up1::default_group,
        require => User[$up1::default_user]
    }

    vcsrepo { $up1::default_path:
        ensure   => 'present',
        provider => 'git',
        source   => $up1::repository_remote,
        revision => $up1::repository_revision,
        owner    => $up1::default_user,
        group    => $up1::default_group,
        require  => [ User[$up1::default_user], Group[$up1::default_group] ],
        notify   => Exec['compile_up1']
    }

    exec { 'compile_up1':
        command     => "/usr/bin/go build -o ${up1::default_path} ${up1::default_path}/server.go",
        user        => $up1::default_user,
        logoutput   => on_failure,
        refreshonly => true
    }

}
